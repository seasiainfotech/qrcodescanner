//
//  ViewController
//
//  Created by Seasia on 10/12/16.
//  Copyright © 2016 Seasia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QRCodeReaderDelegate.h"

@interface ViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,QRCodeReaderDelegate>

@property (nonatomic) IBOutlet UIView *overlayView;
@property(strong,nonatomic)NSDictionary *dictPharmacy;
@end

