//
//  ViewController
//
//  Created by Seasia on 10/12/16.
//  Copyright © 2016 Seasia. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"

#define CAMERA_TRANSFORM_X 1
#define CAMERA_TRANSFORM_Y 1.12412
//iphone screen dimensions
#define SCREEN_WIDTH  320
#define SCREEN_HEIGTH 480

@interface ViewController ()
{
    IBOutlet UILabel *flashBtnLbl;

    __weak IBOutlet UITextField *patName;
    UIImagePickerController *picker;
    IBOutlet UIView *flashOptionView;
    IBOutlet UIImageView *iPhone6PlusImageViewForTakeRetake;
    IBOutlet UIView *cameraView;
    IBOutlet UIView *topCameraView;
    NSString * imagePath;
    UIActivityIndicatorView *objectActivityIndicator;

    __weak IBOutlet NSLayoutConstraint *widthConstant;
}
@property (weak, nonatomic) IBOutlet UIButton *buttonPharmacy;
- (IBAction)action_Pharmacy:(id)sender;
@end

@implementation ViewController
- (NSString *)encodeToBase64String:(UIImage *)image {
    NSData *dataForJPEGFile = UIImageJPEGRepresentation(image, 1 );
    NSString *base64Decoded = [dataForJPEGFile base64EncodedStringWithOptions:0];
    return base64Decoded;

}
-(void)viewDidLayoutSubviews{
    widthConstant.constant=0;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    objectActivityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    objectActivityIndicator.frame = CGRectMake(0.0, 0.0, 80.0, 80.0);
    objectActivityIndicator.center = self.view.center;
    [self.view addSubview: objectActivityIndicator];

    if (self.dictPharmacy) {
        [self.buttonPharmacy setTitle:[self.dictPharmacy valueForKey:@"BusinessName"] forState:UIControlStateNormal];
    }
    
    //[self httpUpload];
    //[self uploadLocalImage];

     // Do any additional setup after loading the view, typically from a nib.
}

-(IBAction)openCameraWithOverlay{
    dispatch_async(dispatch_get_main_queue(), ^{

    picker= [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.showsCameraControls = YES ;
    picker.modalPresentationStyle = UIModalPresentationFullScreen;
    
        [picker setCameraFlashMode:0];
        
        picker.showsCameraControls = NO;
        picker.cameraViewTransform = CGAffineTransformMakeScale(1.33333, 1.33333);
    
        [[NSBundle mainBundle] loadNibNamed:@"OverlayView" owner:self options:nil];
        
        self.overlayView.frame = picker.cameraOverlayView.frame;
        picker.cameraOverlayView = self.overlayView;
        self.overlayView = nil;
        
        
        [self presentViewController: picker
                           animated: YES
                         completion: nil];

    });

}
- (void)imagePickerController:(UIImagePickerController *)picker1 didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
    if (authStatus == ALAuthorizationStatusDenied) {
        
        [[[UIAlertView alloc] initWithTitle:@"Photo's access" message:@"Please allow  access to photo's in setting app to open camera" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        [picker1 dismissViewControllerAnimated:YES completion:nil];
        
        return;
    }

}
#pragma mark UIImagePicker Delegates
- (UIImage *) scaleAndRotateImage: (UIImage *)image
{
    
    int kMaxResolution ;
    kMaxResolution = self.view.frame.size.width *4  ;
   
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution)
    {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    float constantToMultiple = 1;
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width*constantToMultiple, height*constantToMultiple), imgRef);
    
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    return imageCopy;
}


- (IBAction)btnCameraCancelIPhone6Plus:(id)sender {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnFlashIPhone6Plus:(id)sender {
    if (widthConstant.constant ==0) {
        widthConstant.constant =150;
    }else{
        widthConstant.constant =0;
    }
    
}
- (IBAction)btnChangeCameraIPhone6Plus:(id)sender {
    if (picker.cameraDevice == UIImagePickerControllerCameraDeviceRear) {
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }
    else {
        picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
}
- (IBAction)btnPhotoClickIPhone6Plus:(id)sender {
    cameraView.userInteractionEnabled = NO;
    topCameraView.userInteractionEnabled = NO;
    [picker takePicture];
    
}
- (IBAction)btnFlashOptionsClickIPhone6Plus:(UIButton *)sender {
    
    for (UIButton * btn in flashOptionView.subviews) {
        if (btn.tag==-1 || btn.tag ==0 || btn.tag == 1) {
            [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //     [self.picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
    //
    if (sender.tag == 1) {
        flashBtnLbl.text = @"On";
    }
    if (sender.tag == -1) {
        flashBtnLbl.text = @"Off";
    }
    if (sender.tag == 0) {
        flashBtnLbl.text = @"Auto";
    }
    [picker setCameraFlashMode:sender.tag];
    
    flashOptionView.frame = CGRectMake(320, 0, flashOptionView.frame.size.width, flashOptionView.frame.size.height);
}

- (IBAction)btnUsePhotoIphone:(id)sender {
    [self dismisssCustomViewWithPopOutAnimationForWindow];
    
//    [self saveImage:iPhone6PlusImageDictionary] ;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    // [iPhone6PlusImageDictionary removeAllObjects];
    
}
- (IBAction)btnReTake:(id)sender {
    
    [self dismisssCustomViewWithPopOutAnimationForWindow];
    
    //    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)dismisssCustomViewWithPopOutAnimationForWindow
{
    cameraView.userInteractionEnabled = YES;
    topCameraView.userInteractionEnabled = YES;
    AppDelegate * objectAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    for (UIView * dialogView in [objectAppDelegate.window subviews])
    {
        if (dialogView.tag == 1010)
        {
            CATransform3D currentTransform = dialogView.layer.transform;
            
            CGFloat startRotation = [[dialogView valueForKeyPath:@"layer.transform.rotation.z"] floatValue];
            CATransform3D rotation = CATransform3DMakeRotation(-startRotation + M_PI * 270.0 / 180.0, 0.0f, 0.0f, 0.0f);
            
            dialogView.layer.transform = CATransform3DConcat(rotation, CATransform3DMakeScale(1, 1, 1));
            dialogView.layer.opacity = 1.0f;
            
            [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                             animations:^{
                                 
                                 dialogView.layer.transform = CATransform3DConcat(currentTransform, CATransform3DMakeScale(0.6f, 0.6f, 1.0));
                                 dialogView.layer.opacity = 0.0f;
                             }
                             completion:^(BOOL finished){
                                 [dialogView removeFromSuperview];
                                 
                             }];
            
        }
        
    }
    
}

-(void)saveImageToDocumentDirectory:(UIImage *)image
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    imagePath = [NSString stringWithFormat:@"Photo%@.png",[self randomString]];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:imagePath];
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    
    BOOL success = [imageData writeToFile:filePath atomically:YES];
    
    NSLog(@"Successs:::: %@", success ? @"YES" : @"NO");
    if (success)
    {}
//    imagePath = filePath;
    NSLog(@"image path --> %@",filePath );
}


- (IBAction)sendPhoto:(id)sender {
    if (self.dictPharmacy) {
        if (!iPhone6PlusImageViewForTakeRetake.image) {
            return;
        }
        AppDelegate * objectAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (![objectAppDelegate isInternetAvailable]) {
            
            [[[UIAlertView alloc]initWithTitle:@"QRScanner" message:@"Please connect to internet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            return;
        }
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        [objectActivityIndicator startAnimating];
        
        //    saving image using ftp
        [self saveImageToDocumentDirectory:(UIImage *)iPhone6PlusImageViewForTakeRetake.image];
        
        [self dismisssCustomViewWithPopOutAnimationForWindow];
    }
    else{
        [[[UIAlertView alloc]initWithTitle:@"QRScanner" message:@"Please select pharmacy." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        
    }
    
    
    
}
- (IBAction)dicardPhoto:(id)sender {
    [self dismisssCustomViewWithPopOutAnimationForWindow];
}


-(NSString *) randomString {
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"HHmmss"];
    [dateFormat2 setTimeZone:[NSTimeZone timeZoneWithName:@"Australia/Melbourne"]];
    NSString *dateString = [dateFormat2 stringFromDate:[NSDate date]];
    NSLog(@"DateString: %@", dateString);
    return dateString;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - QRCodeScanning

- (IBAction)action_Pharmacy:(id)sender
{
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            
            vc                   = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;
        
        [vc setCompletionWithBlock:^(NSString *resultAsString) {
            NSLog(@"Completion with result: %@", resultAsString);
        }];
        
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Reader not supported by the current device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
}

#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [self dismissViewControllerAnimated:YES completion:^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"QRCodeReader" message:result delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)backBtn:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
